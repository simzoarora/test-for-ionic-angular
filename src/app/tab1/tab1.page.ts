import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
@Injectable({
  providedIn: 'root'
})
export class Tab1Page {
  articles:any;

  constructor(private http: HttpClient) {}
  imageData(){
    return this.http.get<any>(`https://jsonplaceholder.typicode.com/albums/1/photos`);
  }
  doInfinite() {
     setTimeout(() => {
       this.imageData().subscribe(res => {
         for(let i=0 ;i < res.length ; i++){
          this.articles.push(res[i])
         }
       })
     }, 500); }

  ngOnInit() {
    this.imageData().subscribe(res =>{
      this.articles = res;
    });
  }
  

}
