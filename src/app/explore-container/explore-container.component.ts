import { Component, OnInit, Input } from '@angular/core';
import { ViewChild } from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Component({
  selector: 'app-explore-container',
  templateUrl: './explore-container.component.html',
  styleUrls: ['./explore-container.component.scss'],
})
 
@Injectable({
  providedIn: 'root'
})
export class ExploreContainerComponent implements OnInit {

  @ViewChild('slideWithNav2', { static: false }) slideWithNav2: IonSlides;


  sliderTwo: any;

  @Input() name: string;

  //Configuration for each Slider

  slideOptsTwo = {
    initialSlide: 1,
    slidesPerView: 2,
    loop: true,
    centeredSlides: true,
    spaceBetween: 20
  };



  constructor(private http: HttpClient
    ) {
      this.sliderTwo =
      {
        isBeginningSlide: true,
        isEndSlide: false,
        slidesItems: []
      };
    }
  
    //Move to Next slide
    slideNext(object, slideView) {
      slideView.slideNext(500).then(() => {
        this.checkIfNavDisabled(object, slideView);
      });
    }
  
    //Move to previous slide
    slidePrev(object, slideView) {
      slideView.slidePrev(500).then(() => {
        this.checkIfNavDisabled(object, slideView);
      });;
    }
  
    //Method called when slide is changed by drag or navigation
    SlideDidChange(object, slideView) {
      this.checkIfNavDisabled(object, slideView);
    }
  
    //Call methods to check if slide is first or last to enable disbale navigation  
    checkIfNavDisabled(object, slideView) {
      this.checkisBeginning(object, slideView);
      this.checkisEnd(object, slideView);
    }
  
    checkisBeginning(object, slideView) {
      slideView.isBeginning().then((istrue) => {
        object.isBeginningSlide = istrue;
      });
    }
    checkisEnd(object, slideView) {
      slideView.isEnd().then((istrue) => {
        object.isEndSlide = istrue;
      });
    }
    imagesData(){
      return this.http.get(`https://jsonplaceholder.typicode.com/albums/1/photos`);
    }

  ngOnInit() {
    this.imagesData().subscribe(res =>{
      console.log(res, 'data')
      this.sliderTwo.slidesItems = res;
    });
    console.log(this.sliderTwo, 'here')
  }
  
}
